/**
 *
 */
package top.ibase4j.core.support.context;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import top.ibase4j.core.support.rpc.EnableDubboReference;
import top.ibase4j.core.support.rpc.EnableMotan;
import top.ibase4j.core.util.InstanceUtil;

/**
 *
 * @author ShenHuaJie
 * @version 2017年12月6日 上午11:53:31
 */
@Component
public class ApplicationContextHolder implements ApplicationContextAware {
    private static final Logger logger = LogManager.getLogger();
    static ApplicationContext applicationContext;
    private static final Map<String, Object> serviceFactory = InstanceUtil.newHashMap();
    private static final EnableDubboReference enableDubbo = new EnableDubboReference();
    private static final EnableMotan enableMotan = new EnableMotan();

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ApplicationContextHolder.applicationContext = applicationContext;
    }

    public static <T> T getBean(Class<T> t) {
        return applicationContext.getBean(t);
    }

    public static <T> Map<String, T> getBeansOfType(Class<T> t) {
        return applicationContext.getBeansOfType(t);
    }

    public static Object getBean(String name) {
        return applicationContext.getBean(name);
    }

    @SuppressWarnings({"unchecked"})
    public static <T> T getService(Class<T> cls) {
        String clsName = cls.getName();
        T v = (T)serviceFactory.get(clsName);
        if (v == null) {
            synchronized (clsName) {
                v = (T)serviceFactory.get(clsName);
                if (v == null) {
                    logger.info("*****Autowire {}*****", cls);
                    if (enableDubbo.matches(null, null)) {
                        v = DubboContext.getService(cls);
                    } else if (enableMotan.matches(null, null)) {
                        v = MotanContext.getService(cls);
                    } else {
                        v = ApplicationContextHolder.getBean(cls);
                    }
                    logger.info("*****{} Autowired*****", cls);
                    serviceFactory.put(clsName, v);
                }
            }
        }
        return v;
    }
}
